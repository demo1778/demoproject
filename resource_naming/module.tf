
locals {

  resource_name_map = {
    #global settings
    delimiter    = "${local.delimiter}"
    postfix      = "${local.postfix}"
    cloudprefix  = "${var.global_settings.naming.cloudprefix}"
    locationcode = "${var.global_settings.naming.locationcode}"
    envlabel     = "${var.global_settings.naming.envlabel}"
    locationcode = "${var.global_settings.naming.locationcode}"

    #global settings resource group labels
    rgName   = "${try(var.settings.name, "")}"
    rgSuffix = "${var.global_settings.naming.rgsuffix}"

    #global settings resource labels
    adds                       = lookup(var.global_settings.naming.resourcelabels, "adds", "ADDS")
    keyvault                   = lookup(var.global_settings.naming.resourcelabels, "keyvault", "KV")
    vnet                       = lookup(var.global_settings.naming.resourcelabels, "vnet", "VNET")
    subnet                     = lookup(var.global_settings.naming.resourcelabels, "subnet", "SNET")
    nsg                        = lookup(var.global_settings.naming.resourcelabels, "nsg", "NSG")
    nsgflowlogs                = lookup(var.global_settings.naming.resourcelabels, "nsgflowlogs", "NSGFlowLog")
    vng                        = lookup(var.global_settings.naming.resourcelabels, "vng", "VNG")
    vngc                       = lookup(var.global_settings.naming.resourcelabels, "vngc", "VNGConn")
    p2svpn                     = lookup(var.global_settings.naming.resourcelabels, "p2svpn", "P2SVPN")
    vpng                       = lookup(var.global_settings.naming.resourcelabels, "vpng", "VPNG")
    erg                        = lookup(var.global_settings.naming.resourcelabels, "erg", "ERG")
    vwan                       = lookup(var.global_settings.naming.resourcelabels, "vwan", "VWAN")
    vhub                       = lookup(var.global_settings.naming.resourcelabels, "vhub", "VHUB")
    lng                        = lookup(var.global_settings.naming.resourcelabels, "lng", "LNG")
    azfirewall                 = lookup(var.global_settings.naming.resourcelabels, "azfirewall", "AZFW")
    routetable                 = lookup(var.global_settings.naming.resourcelabels, "routetable", "RT")
    bastion                    = lookup(var.global_settings.naming.resourcelabels, "bastion", "BAS")
    rsv                        = lookup(var.global_settings.naming.resourcelabels, "rsv", "RSV")
    loganalytics               = lookup(var.global_settings.naming.resourcelabels, "loganalytics", "LA")
    avset                      = lookup(var.global_settings.naming.resourcelabels, "avset", "AS")
    asg                        = lookup(var.global_settings.naming.resourcelabels, "asg", "ASG")
    nic                        = lookup(var.global_settings.naming.resourcelabels, "nic", "NIC")
    osdisk                     = lookup(var.global_settings.naming.resourcelabels, "osdisk", "OSDisk")
    datadisk                   = lookup(var.global_settings.naming.resourcelabels, "datadisk", "DataDisk")
    publicip                   = lookup(var.global_settings.naming.resourcelabels, "publicip", "PIP")
    publicip_prefix            = lookup(var.global_settings.naming.resourcelabels, "publicip_prefix", "PRFX")
    automationacct             = lookup(var.global_settings.naming.resourcelabels, "automationacct", "AA")
    expressrouteport           = lookup(var.global_settings.naming.resourcelabels, "expressrouteport", "ERP")
    expressroutecircuit        = lookup(var.global_settings.naming.resourcelabels, "expressroutecircuit", "ERC")
    expressroutecircuitpeering = lookup(var.global_settings.naming.resourcelabels, "expressroutecircuitpeering", "ERCP")
    expressroutecircuitauth    = lookup(var.global_settings.naming.resourcelabels, "expressroutecircuitauth", "ERCA")
    expressroutecircuitconn    = lookup(var.global_settings.naming.resourcelabels, "expressroutecircuitconn", "ERCConn")
    expressrouteconnection     = lookup(var.global_settings.naming.resourcelabels, "expressrouteconnection", "ERConn")
    lock                       = lookup(var.global_settings.naming.resourcelabels, "lock", "LOCK")
    azfirewallpolicy           = lookup(var.global_settings.naming.resourcelabels, "azfirewallpolicy", "AZFW-POLICY")
    azfirewallpolicyrcg        = lookup(var.global_settings.naming.resourcelabels, "azfirewallpolicyrcg", "AZFW-POLICY-RCG")
    azfirewallpolicynetrule    = lookup(var.global_settings.naming.resourcelabels, "azfirewallpolicynetrule", "NETWORK-RC")
    azfirewallpolicyapprule    = lookup(var.global_settings.naming.resourcelabels, "azfirewallpolicyapprule", "APPLICATION-RC")
    azfirewallpolicynatrule    = lookup(var.global_settings.naming.resourcelabels, "azfirewallpolicynatrule", "DNAT-RC")
    avd                        = lookup(var.global_settings.naming.resourcelabels, "avd", "AVD")
    vmss                       = lookup(var.global_settings.naming.resourcelabels, "vmss", "VMSS")
    ipgroup                    = lookup(var.global_settings.naming.resourcelabels, "ipgroup", "IPGRP")
    networkwatcher             = lookup(var.global_settings.naming.resourcelabels, "networkwatcher", "NetworkWatcher")
    privateendpoint            = lookup(var.global_settings.naming.resourcelabels, "privateendpoint", "PRVENDPNT")
    dnszonevnetlink            = lookup(var.global_settings.naming.resourcelabels, "dnszonevnetlink", "DNSZONEVNETLNK")
    routefilter                = lookup(var.global_settings.naming.resourcelabels, "routefilter", "RF")

    #resource specific settings    
    name              = "${var.name == null ? try(var.settings.name, "") : var.name}"
    vmnameprefix      = "${var.global_settings.naming.vmnameprefix}"
    referenced_name   = "${try(var.referenced_name, "")}"
    referenced_name_1 = "${try(var.referenced_name_1, "")}"

    vnet_address_space    = can(var.settings.address_space[0]) ? replace("${try(var.settings.address_space[0], "")}", "/", "_") : ""
    snet_address_prefixes = can(var.settings.address_prefixes[0]) ? replace("${try(var.settings.address_prefixes[0], "")}", "/", "_") : ""
  }

  # naming_convention = {
  #   delimiter = lookup(try(var.settings.naming_convention, {}), "delimiter", null) == null ? try(var.global_settings.naming_convention.delimiter, local.delimiter_default) : var.settings.naming_convention.delimiter}
  #   postfix = 
  #   force_lowercase = 
  #   force_uppercase = 
  # }

  #This is a last resort failsafe delimiter
  delimiter_default = "-"

  delimiter = lookup(try(var.settings.naming_convention, {}), "delimiter", null) == null ? lookup(try(var.global_settings.naming.naming_convention, {}), "delimiter", local.delimiter_default) : lookup(try(var.settings.naming_convention, {}), "delimiter", local.delimiter_default)

  postfix = lookup(try(var.settings.naming_convention, {}), "postfix", null) == null && length(regexall("{postfix}", var.name_mask)) > 0 ? tostring(random_integer.default_postfix[0].result) : lookup(try(var.settings.naming_convention, {}), "postfix", null)

  #something to stew on if we ever want to look into a counter
  #postfix = try(var.settings.vnet.naming_convention.postfix, format("%00d", var.index + 1))

  #This bad boy holds the semi-final result exluding the forced upper/lowercase
  name_result_temp = replace(format(
    replace(var.name_mask, "/{(${join("|", keys(local.resource_name_map))})}/", "%s"),
    [
      for value in flatten(regexall("{(${join("|", keys(local.resource_name_map))})}", var.name_mask)) :
      lookup(local.resource_name_map, value)
    ]...
  ), "/", "_")

  name_result = lookup(try(var.global_settings.naming.naming_convention, {}), "force_lowercase", false) == true || lookup(try(var.settings.naming_convention, {}), "force_lowercase", false) == true ? lower(local.name_result_temp) : lookup(try(var.global_settings.naming.naming_convention, {}), "force_uppercase", false) == true || lookup(try(var.settings.naming_convention, {}), "force_uppercase", false) == true ? upper(local.name_result_temp) : local.name_result_temp


}

resource "random_integer" "default_postfix" {
  min   = 1
  max   = 100
  count = lookup(try(var.settings.naming_convention, {}), "postfix", null) == null && length(regexall("{postfix}", var.name_mask)) > 0 ? 1 : 0

  # keepers (Map of String) Arbitrary map of values that, when changed, will trigger recreation of resource
  # keepers = {
  #   # Generate a new integer each time we switch to a new virtual_machine
  #   virtual_machine = "${var.virtual_machine}"
  # }
}
